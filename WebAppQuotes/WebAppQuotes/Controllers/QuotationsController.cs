﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebAppQuotation.Models;
using WebAppQuotes.Models;

namespace WebAppQuotes.Controllers
{
    public class QuotationsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        ApplicationDbContext userdb = new ApplicationDbContext();
       
        // GET: Quotations
        public ActionResult Index(string searchString)
        {

            //var user =manager.FindById(User.Identity.GetUserIdentity());
            //
           /* ViewBag.authorSort = String.IsNullOrEmpty(sortOrder) ? "Author sort" : "";
            ViewBag.nameSort = sortOrder == "Name" ? "Name desc" : "Name";
            */
            var quotations = db.Quotations.Include(q => q.Category);

            var quote = from q in db.Quotations
                        select q;
            if (!String.IsNullOrEmpty(searchString))
            {
                quote = quote.Where(s => s.Author.Contains(searchString) || s.Category.Name.Contains(searchString) || s.Quote.Contains(searchString));
            }
            return View(quote.ToList());

            /*
            switch (sortOrder)
            {
                case "Author sort":
                    quotations = quotations.OrderByDescending(s => s.Author);
                    break;

                case "Author":
                    quotations = quotations.OrderBy(s => s.Author);
                    break;

                case "Name desc":
                    quotations = quotations.OrderByDescending(s => s.Category);
                    break;

                case "Name":
                    quotations = quotations.OrderBy(s => s.Category);
                    break;
                default:
                    quotations = quotations.OrderBy(s => s.Author);
                    break;
            }
            //
            return View(quotations.ToList()); */
        }

        // GET: Quotations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Quotation quotation = db.Quotations.Find(id);
            if (quotation == null)
            {
                return HttpNotFound();
            }
            return View(quotation);
        }

        // GET: Quotations/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name");
            return View();
        }

        // POST: Quotations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "QuotationID,CategoryID,Quote,Author,Date")] Quotation quotation, String Name)
        {
            Category newCategory = new Category();
            //s.Category.Name.Contains(searchString)
            if (!String.IsNullOrEmpty(Name))
            {
                int intId;
                int[] categoryArray = db.Categories.Where(c => c.Name == Name).Select(c => c.CategoryID).ToArray();
                if (categoryArray.Count() > 0)
                {
                    intId = categoryArray[0];
                } else {
                    newCategory.Name = Name;
                    db.Categories.Add(newCategory);
                    intId = newCategory.CategoryID;
                }

                newCategory = db.Categories.Find(intId);
                quotation.CategoryID = newCategory.CategoryID;
            }

            if (ModelState.IsValid)
            {
                
                    quotation.Date = DateTime.Now;
                    db.Quotations.Add(quotation);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                
            }

            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name", quotation.CategoryID);
            return View(quotation);
        }

        // GET: Quotations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Quotation quotation = db.Quotations.Find(id);
            if (quotation == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name", quotation.CategoryID);
            return View(quotation);
        }

        // POST: Quotations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "QuotationID,Quote,Author,CategoryID,Date")] Quotation quotation)
        {
            if (ModelState.IsValid)
            {
                quotation.Date = DateTime.Now;
                db.Entry(quotation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name", quotation.CategoryID);
            return View(quotation);
        }

        // GET: Quotations/Delete/5
        //[Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Quotation quotation = db.Quotations.Find(id);
            if (quotation == null)
            {
                return HttpNotFound();
            }
            return View(quotation);
        }

        // POST: Quotations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Quotation quotation = db.Quotations.Find(id);
            db.Quotations.Remove(quotation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
