﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace WebAppQuotation.Models
{
    public class Quotation
    {
        public int QuotationID { get; set; }

        [Required]
        public string Quote { get; set; }
        [Required]
        public string Author { get; set; }

        public virtual Category Category { get; set; }


        public int CategoryID { get; set; }

    [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "(0:MM/dd/yyyy}",ApplyFormatInEditMode = true)]
        public DateTime Date{get;set;}
    }
}
