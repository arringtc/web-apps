﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace WebAppQuotation.Models
{
    public class Category
    {
        [DisplayName("Category")]
        public int CategoryID { get; set; }

        [DisplayName("Category")]
        public string Name { get; set; }

    }
}