namespace WebAppQuotes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newtime : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Quotations", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Quotations", "Date", c => c.String());
        }
    }
}
